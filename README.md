# LCM Engine
This project has been migrated to Eclipse Foundation, and it can be found under https://gitlab.eclipse.org/eclipse/xfsc/

The implementation of Gaia-X Life-Cycle Management Engine (LCM Engine).

Documentation is available on https://gaia-x.gitlab.io/data-infrastructure-federation-services/orc/documentation/0300-lcm-engine.html
